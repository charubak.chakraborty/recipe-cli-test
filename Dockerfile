# FROM golang:latest

# LABEL maintainer="Charubak Chakraborty <charubak.chakraborty@gmail.com>"

# RUN ls -la

# RUN mkdir /temp

# ARG filename

# WORKDIR /app

# # Copy JSON input into app
# COPY ${filename} /app/temp/input.json

# # Copy all contents
# COPY . .

# # Download all the dependencies
# RUN go get -d -v ./...

# # Install the package
# RUN go install -v ./...

# # Build the app
# RUN go build

# CMD ./recipe-cli-test

#======================

FROM golang:latest

LABEL maintainer="Charubak Chakraborty <charubak.chakraborty@gmail.com>"

RUN mkdir /app

COPY . /app

ARG filename

COPY ${filename} /app/temp/input.json

WORKDIR /app

RUN go get -d -v ./...

RUN go build 

CMD ./app